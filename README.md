# Network path converter

## General Notes
This project enables you to convert network paths pretty easily from MS-DOS/Microsoft Windows style to Unix style.
The times of changing slashes to backslashes and vice versa are gone.  
So you can either convert the path in any direction, or if it is a Windows path you can directly open it without the need of converting the path before.

## Important
The scripts work on MacOS only.  
The scripts do only work in a text editor (NOT VS Code though).

## Installation
- [Download](https://gitlab.com/paessler-labs/file-path-converter/-/archive/master/file-path-converter-master.zip) the contents or clone the repo
- Using precreated workflows: double click on any of the three workflows in folder workflows
- Using precreated workflows: Go to ````System Preferences / Keyboard / Shortcuts```` and make sure the workflows are enabled
- Using precreated workflows: Now bring up the context menu with text highlighted and under Services, the workflows should show up
- Creating own Workflow: 
  + Start automator and start a new project
  + choose ````Quick Action````
  + type Script in the Search bar
  + choose ````Run Applescript````
  + paste the contents of any of the tree files under scripts to the editor window
  + Settings for the action are ````Workflow receives current text in any application````, set the tick at ````Output replaces selected text```` 
  + Save (hint: try saving with a prefix like ````PREFIX_ ACTIONNAME ```` as this will group all the actions in the same section of the context menu)
  + Repeat for all three scripts

### Automator Settings
![Automator Settings](pics/automator_settings.png)

### Keyboard shortcut settings
![Keyboard Settings](pics/keyboard_settings.png)

### Context Menu
![Context Menu](pics/context_menu.png)


## How does it work?
All three scrips are AppleScripts inside a Automator file.

### Path_ Open Windows Path
````applescript
on searchReplace(theText, SearchString, ReplaceString)
    set OldDelims to AppleScript's text item delimiters
    set AppleScript's text item delimiters to SearchString
    set newText to text items of theText
    set AppleScript's text item delimiters to ReplaceString
    set newText to newText as text
    set AppleScript's text item delimiters to OldDelims
    return newText
end searchReplace

on run {input, parameters}
    set myClip to the input
    set mytext to searchReplace(myClip, "&lt;", "")
    set mytext to searchReplace(mytext, "&gt;.", "")
    set mytext to searchReplace(mytext, "&gt;", "")
    set findIt to "\\"
    set replaceIt to "/"
    set mylocation to searchReplace(mytext, findIt, replaceIt)
    set mylocation to "smb:" & mylocation
    tell application "Finder"
        open location mylocation
    end tell
end run
````

### Path_ Convert Apple to Windows
````applescript
on searchReplace(theText, SearchString, ReplaceString)
    set OldDelims to AppleScript's text item delimiters
    set AppleScript's text item delimiters to SearchString
    set newText to text items of theText
    set AppleScript's text item delimiters to ReplaceString
    set newText to newText as text
    set AppleScript's text item delimiters to OldDelims
    return newText
end searchReplace

on run {input, parameters}
    set myClip to the input
    set mytext to searchReplace(myClip, "&lt;", "")
    set mytext to searchReplace(mytext, "&gt;.", "")
    set mytext to searchReplace(mytext, "&gt;", "")
    set mytext to searchReplace(mytext, "smb://", "\\\\")
    set findIt to "/"
    set replaceIt to "\\"
    set mylocation to searchReplace(mytext, findIt, replaceIt)
    return mylocation
end run
````

### Path_ Convert Windows to Apple
````applescript
on searchReplace(theText, SearchString, ReplaceString)
    set OldDelims to AppleScript's text item delimiters
    set AppleScript's text item delimiters to SearchString
    set newText to text items of theText
    set AppleScript's text item delimiters to ReplaceString
    set newText to newText as text
    set AppleScript's text item delimiters to OldDelims
    return newText
end searchReplace

on run {input, parameters}
    set myClip to the input
    set mytext to searchReplace(myClip, "&lt;", "")
    set mytext to searchReplace(mytext, "&gt;.", "")
    set mytext to searchReplace(mytext, "&gt;", "")
    set findIt to "\\"
    set replaceIt to "/"
    set mylocation to searchReplace(mytext, findIt, replaceIt)
    set mylocation to "smb:" & mylocation
    return mylocation
end run
````

## Contributors
Stephan Linke, Paessler AG  
Patrick Niedermayer, Paessler AG  
Konstantin Wolff, Paessler AG  


The project was inspired by [this discussion](https://discussions.apple.com/thread/5630586) in the Apple Help Forums.